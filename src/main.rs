extern crate futures;
extern crate hyper;

use futures::future;
use hyper::rt::{Future, Stream};
use hyper::service::service_fn;
use hyper::{Body, Method, Request, Response, Server, StatusCode, HeaderMap};

//This is a type alias
type BoxFut = Box<Future<Item=Response<Body>, Error=hyper::Error> + Send>;

//Application Main
fn main()
{
	//Set our listening IP and port
	let addr = ([127, 0, 0, 80], 8080).into();

	let server = Server::bind(&addr)
		.serve(|| service_fn(handle_request_function))
		.map_err(|e| eprintln!("server error: {}", e));

	hyper::rt::run(server);
}

fn handle_request_function(req: Request<Body>) -> BoxFut
{
	//For debugging
	println!("METHOD: {}", req.method());
	println!("PATH: {}", req.uri().path());
	println!("QUERYS:");
	for (v) in req.uri().query().iter()
	{
		println!("	{:?}", v);
	}
	println!("HEADERS:");
	for (k, v) in req.headers().iter()
	{
		println!("	{:?} - {:?}", k, v);
	}
	println!("");
	
	//Prep what to put in our response
	let mut headers = HeaderMap::new();
	let mut status = StatusCode::OK;
	let mut body = Body::empty();

	match (req.method(), req.uri().path())
	{
		(&Method::GET, "/") =>
		{
			headers.insert("content-type", "text/html".parse().unwrap());
			body = Body::from("<h4>Our only page</h4>");
		},
		/*(&Method::POST, "/echo") => {
			// Do something else here later
		},*/
		_ =>
		{
			headers.insert("location", "/".parse().unwrap());
			status = StatusCode::FOUND;
		},
	};
	
	let mut response = Response::new(body);
	*response.headers_mut() = headers;
	*response.status_mut() = status;

	Box::new(future::ok(response))
}